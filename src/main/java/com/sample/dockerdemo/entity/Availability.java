package com.sample.dockerdemo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.sample.dockerdemo.dto.EmployeeAvailability;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Availability {
	@Id
	private String employeeId;
	private Boolean available;
	private String latitude;
	private String longitude;
}
