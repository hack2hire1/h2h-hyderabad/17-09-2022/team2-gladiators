package com.sample.dockerdemo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.sample.dockerdemo.dto.EmployeeAvailability;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
	@Id
	private String employeeId;
	private String employeeName;
	private String managerId;
	private String designation;
}
