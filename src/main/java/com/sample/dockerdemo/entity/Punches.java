package com.sample.dockerdemo.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sample.dockerdemo.dto.EmployeeAvailability;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Punches {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String employeeId;
	private String date;
	private Timestamp punchinTime;
	private Timestamp punchoutTime;
}
