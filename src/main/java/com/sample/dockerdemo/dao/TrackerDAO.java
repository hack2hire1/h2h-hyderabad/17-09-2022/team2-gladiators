package com.sample.dockerdemo.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sample.dockerdemo.dto.EmployeeAvailability;
import com.sample.dockerdemo.dto.EmployeePunchFeed;
import com.sample.dockerdemo.entity.Availability;
import com.sample.dockerdemo.entity.Employee;
import com.sample.dockerdemo.repository.AvailabilityRepository;
import com.sample.dockerdemo.repository.EmployeeRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class TrackerDAO {
	@Autowired
	private AvailabilityRepository availabilityRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public List<EmployeeAvailability> getEmployeeAvailabilityReportingToManager(String managerId){
		List<EmployeeAvailability> employeeAvailability = new ArrayList<>();
		List<Employee> employees = employeeRepository.findByManagerId(managerId);
		List<Availability> availabilityList = new ArrayList<>();
		Map<String, String> idVsNameMap = new HashMap<String, String>();
		for(Employee employee: employees) {
			idVsNameMap.put(employee.getEmployeeId(), employee.getEmployeeName());
			availabilityList.add(availabilityRepository.findByEmployeeId(employee.getEmployeeId()));
		}
		for(Availability availability: availabilityList) {
			employeeAvailability.add(
					EmployeeAvailability.builder()
					.employeeId(availability.getEmployeeId())
					.employeeName(idVsNameMap.get(availability.getEmployeeId()))
					.availability(availability.getAvailable())
					.comments("")
					.latitude(availability.getLatitude())
					.longitude(availability.getLongitude())
					.build()
					);
		}
		return employeeAvailability;
	}
	
	public String updateEmployeeAvailability(EmployeePunchFeed punchFeed) {
		String response = "";
		try {
			Availability availability = availabilityRepository.findByEmployeeId(punchFeed.getEmployeeId());
			if (!Objects.isNull(availability)) {
				if (punchFeed.getPunchIn()) {
					availability.setAvailable(true);
				} else if (punchFeed.getPunchOut()) {
					availability.setAvailable(false);
				}
				availabilityRepository.save(availability);
			} else {
				Availability availabilityOfEmployee = Availability.builder()
						.employeeId(punchFeed.getEmployeeId())
						.available(true)
						.latitude(punchFeed.getGeoLat())
						.longitude(punchFeed.getGeoLong())
						.build();
				availabilityRepository.save(availabilityOfEmployee);
			}
			response = "successfully updated punch";
		} catch (Exception e) {
			log.error(e.getMessage());
			response = "failed to update punch";
		}
		return response;
	}
}
