package com.sample.dockerdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sample.dockerdemo.dao.TrackerDAO;
import com.sample.dockerdemo.dto.EmployeeAvailability;
import com.sample.dockerdemo.dto.EmployeePunchFeed;

@RestController
public class TrackerController {
	
	@Autowired
	TrackerDAO trackerDAO;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/feed/{managerId}")
	public List<EmployeeAvailability> getEmployeeFeed(@PathVariable("managerId") String managerId){
		return trackerDAO.getEmployeeAvailabilityReportingToManager(managerId);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("feed")
	public String postEmployeeFeed(@RequestBody EmployeePunchFeed punchFeed) {
		return trackerDAO.updateEmployeeAvailability(punchFeed);
	}
}
