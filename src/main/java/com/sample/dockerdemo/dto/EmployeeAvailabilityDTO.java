package com.sample.dockerdemo.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeAvailabilityDTO {
	List<EmployeeAvailability> employeeAvailability;
}
