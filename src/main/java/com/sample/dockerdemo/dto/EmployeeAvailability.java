package com.sample.dockerdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeAvailability {
	private String employeeId;
	private String employeeName;
	private Boolean availability;
	private String comments;
	private String latitude;
	private String longitude;
}
