package com.sample.dockerdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeePunchFeed {
	private String employeeId;
	private Boolean punchIn;
	private Boolean punchOut;
	private String comments;
	private String geoLat;
	private String geoLong;
}
