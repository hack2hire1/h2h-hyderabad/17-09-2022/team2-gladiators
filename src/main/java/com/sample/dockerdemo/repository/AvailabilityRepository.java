package com.sample.dockerdemo.repository;

import org.springframework.data.repository.CrudRepository;

import com.sample.dockerdemo.entity.Availability;

public interface AvailabilityRepository extends CrudRepository<Availability, String>{
	public Availability findByEmployeeId(String employeeId);
}
