package com.sample.dockerdemo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sample.dockerdemo.entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, String>{
	public List<Employee> findByManagerId(String managerId);
}
