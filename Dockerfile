# Build stage
FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD
COPY src /home/app/src
COPY pom.xml /home/app
COPY Dockerfile /home/app
WORKDIR /home/app
RUN mvn dependency:go-offline -B
RUN mvn package -DskipTests=true

# Package stage
FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
WORKDIR /app
COPY --from=MAVEN_BUILD /home/app/target/*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]